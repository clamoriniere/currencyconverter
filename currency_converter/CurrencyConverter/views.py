from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from currency_converter.CurrencyConverter.models import CurrencyRate, CurrencyConvertResponse
from currency_converter.CurrencyConverter.serializers import CurrencyRateSerializer, CurrencyConverterResponseSerializer, CurrencyConverterRequestSerializer
from currency_converter.CurrencyConverter.converter import Converter


@api_view(['POST'])
def rate_add(request):
    """
    Add Currency Rate to the database
    """
    if request.method == 'POST':
        request_serializer = CurrencyRateSerializer(data=request.data)
        if request_serializer.is_valid():
            request_serializer.save()
            return Response(request_serializer.data, status=status.HTTP_201_CREATED)
        return Response(request_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def rate_list(request):
    """
    Returns list available currency rate
    """
    if request.method == 'GET':
        rates = CurrencyRate.objects.all()
        serializer = CurrencyRateSerializer(rates, many=True)
        return Response(serializer.data)


@api_view(['POST'])
def currency_convert(request):
    """
    Currency converter service
    """
    if request.method == 'POST':
        request_serializer = CurrencyConverterRequestSerializer(data=request.data)
        if not request_serializer.is_valid():
            return generate_error_response()

        converter_request = request_serializer.save()
        converter = Converter()
        try:
            result = converter.parse(converter_request.query)
            if result is None:
                raise ValueError("Invalid Query")
            return generate_response(result)
        except:
            return generate_error_response()
    else:
        return generate_error_response()


def generate_error_response():
    """
    Generates an error Response
    """
    response = CurrencyConvertResponse(answer="I' sorry Dave. I'm afraid. I can't do that")
    serializer = CurrencyConverterResponseSerializer(response)
    return Response(serializer.data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


def generate_response(answer):
    """
    Generates a valid response
    """
    response = CurrencyConvertResponse(answer=answer)
    serializer = CurrencyConverterResponseSerializer(response)
    return Response(serializer.data)

