import ply.lex as lex
import ply.yacc as yacc

from currency_converter.CurrencyConverter.models import CurrencyRate

class Parser:
    """
    Base class for a lexer/parser that has the rules defined as methods
    """
    tokens = ()

    def __init__(self, **kw):
        self.debug = kw.get('debug', 0)

        # Build the lexer and parser
        lex.lex(module=self, debug=self.debug)
        yacc.yacc(module=self, debug=self.debug)

    def parse(self, string):
        return yacc.parse(string)


class Converter(Parser):
    """
    Converts the query sentence to response sentence
    """
    tokens = ('AMOUNT', 'CURRENCY', 'OPERATOR')

    # Tokens
    t_ignore = ' \t'
    t_CURRENCY = r'[A-Z]{3}'
    t_OPERATOR = r'en'
    t_AMOUNT = '\d+(\.\d+)?'

    def t_error(self, t):
        raise TypeError("Unknown text '%s'" % (t.value,))

    def p_error(self, p):
        if p:
            print("Syntax error at '%s'" % p.value)
        else:
            print("Syntax error at EOF")

    def p_total(self, p):
        "total : AMOUNT CURRENCY OPERATOR CURRENCY"
        try:
            currency_rate = CurrencyRate.objects.get(currency_from=p[2], currency_to=p[4])
        except:
            raise ValueError("CurrencyRate not available in DB")
        value = float(p[1]) * currency_rate.rate
        seq = [str(p[1]), p[2], "=", str(round(value, 2)), p[4]]
        p[0] = " ".join(seq)
