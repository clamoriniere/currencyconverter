from django.test import TestCase
from currency_converter.CurrencyConverter.converter import Converter
from currency_converter.CurrencyConverter.models import CurrencyRate


class ConverterCase(TestCase):
    def setUp(self):
        CurrencyRate.objects.create(currency_from="EUR", currency_to="USD", rate=2)
        CurrencyRate.objects.create(currency_from="USD", currency_to="EUR", rate=0.5)

    def test_nominal_case(self):
        converter = Converter(debug=0)
        try:
            result = converter.parse("10.5 EUR en USD")
        except TypeError:
            self.fail("converter.run should not raise")
        self.assertEqual(result, "10.5 EUR = 21.0 USD")

    def test_empty_input(self):
        converter = Converter(debug=0)
        try:
            result = converter.parse("")
        except:
            self.fail("converter.run should not raise")
        self.assertEqual(result, None)

    def test_bad_input(self):
        converter = Converter(debug=0)
        try:
            result = converter.parse("10.32 EUR en USD dfsdfsf")
        except:
            return
        self.assertEqual(result, None)

    def test_rate_dont_exist(self):
        converter = Converter(debug=0)
        try:
            result = converter.parse("10.32 EUR en LON")
        except:
            return
        self.assertEqual(result, None)
