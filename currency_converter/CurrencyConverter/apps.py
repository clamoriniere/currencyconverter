from __future__ import unicode_literals

from django.apps import AppConfig


class CurrencyConverterConfig(AppConfig):
    name = 'currency_converter'
