from __future__ import unicode_literals
from django.db import models


class CurrencyRate(models.Model):
    """
    Model for storing a "CurrencyRate"
    """
    currency_from = models.CharField(max_length=3)
    currency_to = models.CharField(max_length=3)
    rate = models.FloatField()


class CurrencyConvertRequest(object):
    """
    Model for Deserializing the CurrencyConvertRequest
    """
    def __init__(self, query):
        self.query = query


class CurrencyConvertResponse(object):
    """
    Model for Serializing the CurrencyConvertResponse
    """
    def __init__(self, answer):
        self.answer = answer

