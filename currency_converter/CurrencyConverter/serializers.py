from rest_framework import serializers
from currency_converter.CurrencyConverter.models import CurrencyRate, CurrencyConvertRequest, CurrencyConvertResponse


class CurrencyRateSerializer(serializers.ModelSerializer):
    """
    CurrencyRate Serializer
    """
    class Meta:
        model = CurrencyRate
        fields = ('currency_from', 'currency_to', 'rate')

    def create(self, validated_data):
        return CurrencyRate.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.currency_from = validated_data.get('currency_from', instance.currency_from)
        instance.currency_to = validated_data.get('currency_to', instance.currency_to)
        instance.rate = validated_data.get('rate', instance.rate)
        instance.save()
        return instance


class CurrencyConverterRequestSerializer(serializers.Serializer):
    """
    CurrencyConverterRequest Serializer
    """
    query = serializers.CharField()

    def create(self, validated_data):
        return CurrencyConvertRequest(**validated_data)

    def update(self, instance, validated_data):
        instance.query = validated_data.get('query', instance.query)
        instance.save()
        return instance


class CurrencyConverterResponseSerializer(serializers.Serializer):
    """
    CurrencyConverterResponse Serializer
    """
    answer = serializers.CharField()

    def create(self, validated_data):
        return CurrencyConvertResponse(**validated_data)

    def update(self, instance, validated_data):
        instance.answer = validated_data.get('answer', instance.answer)
        instance.save()
        return instance
