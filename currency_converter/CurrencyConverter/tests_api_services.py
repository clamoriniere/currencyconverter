from rest_framework.test import APITestCase
from currency_converter.CurrencyConverter.views import *
import json


class ConverterCase(APITestCase):

    def test_add_rate(self):
        """
        Validates adding new currency rate
        """
        response = self.client.post('/rates/add', {'currency_from': 'USD', 'currency_to': 'EUR', 'rate': 0.5}, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CurrencyRate.objects.count(), 1)
        self.assertEqual(CurrencyRate.objects.get().currency_from, 'USD')
        self.assertEqual(CurrencyRate.objects.get().currency_to, 'EUR')
        self.assertEqual(CurrencyRate.objects.get().rate, 0.5)

    def test_add_rate_bad_request(self):
        """
        Validates bad request currency rate
        """
        response = self.client.post('/rates/add', {'currency_from': 'USD'},
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_add_rate_bad_http_method(self):
        """
        Validates bad request currency rate
        """
        response = self.client.get('/rates/add')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_list_rate_request(self):
        """
        Validates list currency rates service
        """
        CurrencyRate.objects.create(currency_from="EUR", currency_to="USD", rate=2)

        response = self.client.get('/rates')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.content, '[{"currency_from":"EUR","currency_to":"USD","rate":2.0}]')

        CurrencyRate.objects.create(currency_from="USD", currency_to="EUR", rate=0.5)
        response = self.client.get('/rates')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.content, '[{"currency_from":"EUR","currency_to":"USD","rate":2.0},{"currency_from":"USD","currency_to":"EUR","rate":0.5}]')

    def test_money_convert_request(self):
        """
        Validates list currency rates service
        """
        CurrencyRate.objects.create(currency_from="EUR", currency_to="USD", rate=2)
        response = self.client.post('/money/convert', {'query': '10.32 EUR en USD'}, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_money_convert_bad_request(self):
        """
        Validates list currency rates service
        """
        CurrencyRate.objects.create(currency_from="EUR", currency_to="USD", rate=2)
        response = self.client.post('/money/convert', {'query': '10.32 EUR bad request'}, format='json')

        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)
