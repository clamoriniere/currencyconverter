from django.conf.urls import url
from currency_converter.CurrencyConverter import views

urlpatterns = [
    url(r'^money/convert$', views.currency_convert),
    url(r'^rates/add$', views.rate_add),
    url(r'^rates$', views.rate_list),
]