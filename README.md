# Lengow Currency Rate service

## Services

### Add Currency Rate

* /rates/add (POST): add a CurrencyRate in the application db.

```
Request:
POST /rates/add
{
    "currency_from": "USD",
    "currency_to": "EUR",
    "rate":0.8
}

Response:
201
{
    "currency_from": "USD",
    "currency_to": "EUR",
    "rate":0.8
}
```

### List Currency Rates

* /rates (GET): returns available list of CurrencyRate


```
Request:
GET /rates

Response:
200
[
    { "currency_from": "USD", "currency_to": "EUR", "rate":0.8},
    { "currency_from": "EUR", "currency_to": "USD", "rate":1.2}
]
```

### Money Convert

* /money/convert (POST): from a currency conversion request sentence returns
      an answer sentence that represents the currency ratio


```
Request:
POST /rates
{
    "query":"100 EUR en USD"
}

Response:
200
{
    "answer":"100 EUR = 120 USD"
}
```

## How to install

First you need to setup the virualenv. Inside this folder

```
source env/bin/activate
```

I didn't include the database (db.sqlite3), so yo need to create it

```
./manage.py migrate
```

Now you can run the unit-tests, to validate the installation

```
./manage.py test
```

## Run CurrencyConverter Services

Nothing special just run

```
./manage.py runserver
```

## Test it

In another terminal you can run the following curl command to test the services.

### Add currency rate service

```
clamoriniere:~ $ curl -H "Content-Type: application/json" -X POST -d '{ "currency_from": "USD", "currency_to": "EUR", "rate":0.5 }' http://127.0.0.1:8000/rates/add
{"currency_from":"USD","currency_to":"EUR","rate":0.8}
```

### List currency rates service

```
clamoriniere:~ $ curl -H "Content-Type: application/json" http://127.0.0.1:8000/rates
[{"currency_from":"USD","currency_to":"EUR","rate":0.8},{"currency_from":"EUR","currency_to":"USD","rate":1.2}]
```

### Money Convert

* Valid request:
    ```
    clamoriniere:~ $ curl -H "Content-Type: application/json" -X POST -d '{ "query": "10.32 EUR en USD" }' http://127.0.0.1:8000/money/convert
    {"answer":"10.32 EUR = 8.26 USD"}%
    ```

* Bad request:
    ```
    clamoriniere:~ $ curl -H "Content-Type: application/json" -X POST -d '{ "query": "10.32 EUR bad request" }' http://127.0.0.1:8000/money/convert
    {"answer":"I' sorry Dave. I'm afraid. I can't do that"}%
    ```

